function dynamics = act2dyn(action, Params)

% NYX modified 12/01/2017 17:08;
% dyn = [a_x, v_y];

% dynamics = [0, 0];
switch action
    case 1
        dynamics = [Params.accel_hard, 0, 8];

    case 2
        dynamics = [Params.accel_hard, 0, 9];

    case 3
        dynamics = [Params.accel_hard, 0, 10];
        
    case 4
        dynamics = [Params.accel_mild, 0, 8];
        
    case 5
        dynamics = [Params.accel_mild, 0, 9];

    case 6
        dynamics = [Params.accel_mild, 0, 10];
     
    case 7
        dynamics = [0, 0, 8];

    case 8
        dynamics = [0, 0, 9];

    case 9
        dynamics = [0, 0, 10];

    case 10
        dynamics = [Params.decel_mild, 0, 8];
        
    case 11
        dynamics = [Params.decel_mild, 0, 9];

    case 12
        dynamics = [Params.decel_mild, 0, 10];

    case 13
        dynamics = [Params.decel_hard, 0, 8];

    case 14
        dynamics = [Params.decel_hard, 0, 9];

    case 15
        dynamics = [Params.decel_hard, 0, 10];
    
    case 16
        dynamics = [0, Params.lat_vel, 8];

    case 17
        dynamics = [0, Params.lat_vel, 9];

    case 18
        dynamics = [0, Params.lat_vel, 10];
		
	case 19
        dynamics = [0, Params.lat_vel, 8];

 	case 20
        dynamics = [0, Params.lat_vel, 9];

	case 21
        dynamics = [0, Params.lat_vel, 10];
		%dynamics = [0,0,0]; base station selection. it is override by the update dynamics
	
    otherwise
        error('Unknown action.')
end
