% lisheng 12/4/17
% This is actually a simulator

function [Q, score,ho_density,DR_average,R_transportation,R_telecome,R,Average_Speed,v_array_q,ho_array_q,ho_density_array_q, collision_flag,average_telecome_reward,average_transport_reward,average_Dr_per_meter,time,transport_reward_total,telecome_reward_total,ho_prob] ... 
    = Q_learning(Q, num_env_cars, plot_flag, epsilon, gamma, alpha)
%%
setGlobal;

%% initialization
% Initizlie and Construct the traffic environment
Cars = simulator_initializaiton(Params, num_env_cars);


time = 0;
dist = 0;
score = 0;
ho_prob = 0;

HO_total = 0;
ho_density = 0;
DR_average = 0;
Average_Speed = 0;
% ho_prob = 0;

transport_reward_total = 0 ;
telecome_reward_total = 0 ;
average_telecome_reward = 0;
average_transport_reward =0;
average_Dr_per_meter = 0;

v_array_q = zeros(1,Params.horizon);
ho_array_q = zeros(1,Params.horizon) ; %100 is penalized factors
ho_density_array_q = ones(1,Params.horizon);



ego_obs = get_Observation(1, Cars, Params);
collision_flag = check_collision(Cars, Params);



%SINR set up
NTHzT = 10;%10
NRF = 5;
% R_max = 500;%1000 Params.nominal_speed * Params.horizon
road_length = 1400 ;%Params.nominal_speed * Params.horizon

% Xb = R_max * rand(NTHzT,1)-250;
% Xb = -250:R_max/NTHzT:250-1;

Xb = 0:road_length/NTHzT:road_length-1;
Yb = 20 * logical(randi(2, [1 NTHzT]) - 1) -10;
Yb = Yb';

% Xrb = R_max * rand(NRF,1)-250;
% Xrb = -250:R_max/NRF:250-1;
Xrb = 0:road_length/NRF:road_length-1;
Yrb = 20 * logical(randi(2, [1 NRF]) - 1) -10;
Yrb = Yrb';


if plot_flag
    Plot_Traffics_not_saving_gif(Cars, time, dist, Params, ego_obs,Xb,Yb,Xrb,Yrb);
end

%% Run with dynamics for a given time horizon
% beta version, ego car uses policy 0

for time = 1 : Params.time_step : Params.horizon %200 _>35 30m/s *200s =6000m 30m/s *35s =1050m  0-1000m 1000-6000
    %% update dynamics, with relative coord, i.e. ego_x = 0;
    s = obs2state(ego_obs, Params);
    ego_obs_t = ego_obs;
%     R = get_reward(ego_obs, Cars(1), collision_flag, Params);
    
    % update Cars
    [Cars, dist, ego_action,ho,~,~] = Q_update_dynamics(Cars, dist, Params, Q, epsilon,Xb,Yb,Xrb,Yrb,NTHzT,NRF,ho_prob,time);
    
    % statistics
%    N(s, ego_action) = N(s, ego_action) + 1;
    HO_total = HO_total +ho;
  
    
    % check if there is collision:
    collision_flag = check_collision(Cars, Params);
    
    % update ego_obs
    % ego_obs of next time step:
    ego_obs = get_Observation(1, Cars, Params); % introduce ho_prob to Q_update_dynamics
    ho_prob = min(1,HO_total/time);
    [R_transportation,R_telecome,R] = get_reward(ego_obs_t, Cars(1), collision_flag, Params,ho_prob);
   
    transport_reward_total = transport_reward_total + R_transportation;
    telecome_reward_total = telecome_reward_total + R_telecome;


    a = ego_action;
    % update state to next state
    s_n = obs2state(ego_obs, Params);
    
   %% 
    if collision_flag == false
        Q(s,a) = Q(s,a) + alpha * (R + gamma * max(Q(s_n,:)) - Q(s,a));
        
        if plot_flag
            Plot_Traffics_not_saving_gif(Cars, time, dist, Params, ego_obs,Xb,Yb,Xrb,Yrb);
        end
    else
        % filter bad initialization
        if time >= 3
            
            Q(s,a) = Q(s,a) + alpha * (R + gamma * max(Q(s_n,:)) - Q(s,a));
            score = (dist-4000)/1000;
            ho_prob = min(1,HO_total/time);
            Average_Speed  = dist/time;
            average_telecome_reward = telecome_reward_total/time;
            average_transport_reward = transport_reward_total/time;


            disp('Collision!');
            fprintf('Distance Travelled = %3.4d m\n', dist);
            fprintf('Average Speed      = %3.2f m/s\n', dist/time);
            fprintf('Score              = %3.2f \n', score);
			fprintf('HO  prob           = %3.2f \n', ho_prob);
            fprintf('Average telecome reward              = %8.2f \n', average_telecome_reward);
            fprintf('Average transport reward              = %8.2f \n', average_transport_reward);
            if plot_flag
                Plot_Traffics_not_saving_gif(Cars, time, dist, Params, ego_obs,Xb,Yb,Xrb,Yrb);
            end
        else
            disp('Bad initialization!');
        end
        
        break; % break the current episode & restart simulation
        
    end % collision check
    
end % for time

%%
if collision_flag == false
    score = (dist-4000)/1000;

    ho_density = HO_total/dist ; % instant velocity  * dist/time
    Average_Speed  = dist/time;
    average_telecome_reward = telecome_reward_total/time;
    average_transport_reward = transport_reward_total/time;
    ho_prob = min(1,HO_total/time);


    disp('No Collision');
    fprintf('Distance Travelled = %3.4d m\n', dist);
    fprintf('Average Speed      = %3.2f m/s\n', dist/time);
    fprintf('Score              = %3.2f \n', score);
    fprintf('HO total                = %3.2f \n', HO_total);
    fprintf('Average telecome reward              = %8.2f \n', average_telecome_reward);
    fprintf('Average transport reward              = %8.2f \n', average_transport_reward);
	fprintf('HO  prob           = %3.2f \n', ho_prob);
end

return



