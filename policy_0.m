function action_id = policy_0(obs, Params)

% NYX 11/25/19:09
% lisheng 11/27/17

action_id = randi([7,9],1); % maintain_id 3

% vert hard deceleration to avoid collision
if obs.fc_d < Params.carlength + Params.safe_dist
    action_id = randi([19,21],1); %7;
    return
end

if obs.fc_d <= Params.close_distance && obs.fc_v < 0
    action_id = randi([13,15],1); % decel_hard_id 5
    return
end

if obs.fc_d > Params.close_distance && obs.fc_d <= Params.far_distance
    action_id = randi([10,12],1); % decel_mild_id 4
    return
end

if obs.fc_d > Params.far_distance && obs.fc_v > 0
    action_id = randi([4,6],1); % accel_mild_id
    return
end


% if obs.bs_c == 1 || 3 %1 0 0 or 1 1 0 or 1 1 1
%     action_id = 8;
%     return
% end
% 
% if obs.bs_c == 2 % 0 0 0 
%     action_id = 9;
%     return
% end


return


%% Action Dictionary

% accel_hard_id = 1;
% accel_mild_id = 2;
% maintain_id   = 3;
% decel_mild_id = 4;
% decel_hard_id = 5;
% lane_change   = 6;
% Define action consts
%            1    2   3     4   5    6    7     8       9   10
% actions = [4; 2.5;  0; -2.5; -4; 3.6]; stop  max_bs  bs2 bs3

