
% cd Parameters/
% Q_0 = csvread('Q_alpha_0.2_episode_10000_w5_1.csv');
% cd ..

MAX_STATES_NUM = 3^7 *2; %3^6 * 2 *3;
ACTION_NUM = 21;%7
Q_0 = zeros(MAX_STATES_NUM, ACTION_NUM);



setting_str = 'test92v30w6_alpha_0.005_episode_10000_test_with_ho_room';

episode_limit = 10000;

time_limit    = 9*3600;     % in [seconds]
plot_flag     = false;  % usually not plot during training
epsilon       = 0.1;      % eps-greedy
if_eps_decay  = false;   % if anneal epsilon
decay_rate    = 20;      % greater decay_rate, faster eps decays
gamma         = 0.9;    % discount factor
alpha         = 0.005;    % learning rate


train_Q(Q_0, setting_str, episode_limit, time_limit, plot_flag,...
        epsilon, if_eps_decay, decay_rate, gamma, alpha);
