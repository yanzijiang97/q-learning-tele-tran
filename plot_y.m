function plot_y(dr, episode, collision_flag, epsilon,plot_y,n)
figure(n)
if dr ~= 0
    if collision_flag == true
        plot(episode, dr, '.r');
    else
        plot(episode, dr, '.b');
    end
end
% grid;
hold on;

if episode == 1
    xlabel('episode');
    ylabel(plot_y);
end

legend(['eps = ' num2str(epsilon)]);
drawnow;

end
