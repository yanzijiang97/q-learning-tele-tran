
function train_Q(Q, setting_str, episode_limit, time_limit, plot_flag,...
                    epsilon, if_eps_decay, decay_rate, gamma, alpha)



score = zeros(episode_limit, 1);
ho_density = zeros(episode_limit, 1);
DR_average = zeros(episode_limit, 1);



average_telecome_reward= zeros(episode_limit, 1);
average_transport_reward=zeros(episode_limit, 1);
average_total_reward=zeros(episode_limit, 1);
transport_reward_total = zeros(episode_limit,1);
telecome_reward_total = zeros(episode_limit,1);
time = zeros(episode_limit,1);
average_Dr_per_meter = zeros(episode_limit,1);
ho_prob = zeros(episode_limit,1);


Average_Speed = zeros(episode_limit, 1);

K = decay_rate / episode_limit;


start_time = tic;
for episode = 1 : episode_limit
    
    % epsilon anealing
    if if_eps_decay
        epsilon = 1 * exp(- K * episode);
    end
    
    fprintf('\nepisode = %d\n', episode);
    num_env_cars = randi(5) + 20;

% function [Q, score,ho_density,DR_average,R_transportation,R_telecome,R,Average_Speed,v_array_q,ho_array_q,ho_density_array_q, collision_flag, N,average_telecome_reward,average_transport_reward,average_Dr_per_meter,time,transport_reward_total] ... 
%     = Q_learning(Q, N, num_env_cars, plot_flag, epsilon, gamma, alpha)
    
    [Q, score(episode),ho_density(episode),DR_average(episode),~,~,~,Average_Speed(episode),~,~,~, collision_flag,average_telecome_reward(episode),average_transport_reward(episode),average_Dr_per_meter(episode),time(episode),transport_reward_total(episode),telecome_reward_total(episode),ho_prob(episode)] ...
        = Q_learning(Q, num_env_cars, plot_flag, epsilon, gamma, alpha);
    



    plot_y(score(episode), episode, collision_flag, epsilon,'score',1); 
    plot_y(average_telecome_reward(episode), episode, collision_flag, epsilon,'average telecome reward',5);


    average_total_reward(episode)=average_telecome_reward(episode)+average_transport_reward(episode);


    
    if mod(episode, 500) == 0
        s1 = int2str(episode);
        s2 = 'episode_';
        s = strcat(s2,s1);
        
        set_str = strcat(s,setting_str);
        
        cd Parameters
        csvwrite(['Q_' set_str '.csv'], Q);
        csvwrite(['score_' set_str '.csv'], score);
        csvwrite(['ho_density_' set_str '.csv'], ho_density);
        csvwrite(['speed_average_' set_str '.csv'], Average_Speed);
        csvwrite(['tele_reward_average_' set_str '.csv'], average_telecome_reward);
        csvwrite(['tran_reward_average_' set_str '.csv'], average_transport_reward);
        csvwrite(['total_reward_average_' set_str '.csv'], average_total_reward);
		csvwrite(['time_' set_str '.csv'], time);
		csvwrite(['ho_prob_' set_str '.csv'], ho_prob);
        [~, Policy_1] = max(Q,[],2);
        csvwrite(['Policy_1_' set_str '.csv'], Policy_1);
        cd ..



    end
    
    if toc(start_time) > time_limit
        break;
    end
    
    if plot_flag == true
        close all;
    end
    
end % for episode

cd Parameters
csvwrite(['Q_' setting_str '.csv'], Q);
csvwrite(['score_' setting_str '.csv'], score);
csvwrite(['ho_density_' setting_str '.csv'], ho_density);
csvwrite(['average_speed_' setting_str '.csv'], Average_Speed);
csvwrite(['tele_reward_average_' setting_str '.csv'], average_telecome_reward);
csvwrite(['tran_reward_average_' setting_str '.csv'], average_transport_reward);
csvwrite(['total_reward_average_' setting_str '.csv'], average_total_reward);
csvwrite(['time_' setting_str '.csv'], time);
csvwrite(['ho_prob_' setting_str '.csv'], ho_prob);
[~, Policy_1] = max(Q,[],2);
csvwrite(['Policy_1_' setting_str '.csv'], Policy_1);
cd ..

figure;

